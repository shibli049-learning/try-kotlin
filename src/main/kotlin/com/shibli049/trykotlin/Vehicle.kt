package com.shibli049.trykotlin

/**
 * Created by github.com/shibli049 on 3/14/18.
 */
interface Vehicle {

    val makeName: String
    val startSound: String
    val stopSound: String

    fun go() = println("starting: $startSound")
    fun stop() = println("stopping: $stopSound")

    fun getMpg(): Int
    fun getDoors(): Int
}