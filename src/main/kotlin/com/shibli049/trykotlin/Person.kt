package com.shibli049.trykotlin

/**
 * Created by github.com/shibli049 on 3/13/18.
 */
class Person internal constructor(var firstName: String, var lastName: String){
    init {
        println("Creaating a person, name: $firstName $lastName")
    }

    var middleName: String = ""
    constructor(firstName: String, middleName: String, lastName: String):
            this(firstName, lastName){
        this.middleName = middleName
        println("middlename: $middleName")
    }
}