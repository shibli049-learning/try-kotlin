package com.shibli049.trykotlin

/**
 * Created by github.com/shibli049 on 3/14/18.
 */
class Toyota : Vehicle{
    override val makeName = "Toyota"
    override val startSound = "Bhroom bhrom"
    override val stopSound = "Screetch"

    override fun getMpg(): Int {
        return 50
    }

    override fun getDoors() = 4

}