package com.shibli049.trykotlin

import com.shibli049.trykotlin.java.Animal
import java.util.ArrayList

/**
 * Created by github.com/shibli049 on 3/11/18.
 */


fun main(args: Array<String>) {


    useFoo()
            .stream()
            .forEach { t -> println(t) }

    val c = ArrayList<String>()
    c.add("hello")
    c.add("world")
    c.add("from")
    c.add("shibli")

    c.stream().forEach { t -> println(t) }

    val anInt: Int = 1
    val aLong: Long = anInt.toLong()
    val a = 5
    val b = 3


    //==========================================
    /* conditions */
    printMessage("conditions")

    //java: int min = (a < b) ? a : b;

    val min = if (a < b) a else b
    println("min is $min")


    val mark = 30

    val grade = if (mark >= 80) {
        println("excellent")
        "A"
    } else if (mark >= 60) {
        println("good")
        "B"
    } else {
        println("not good")
        "C"
    }

    println("Grade is $grade")


    val mark2 = 80

    val grade2 = when (mark2) {
        in 80..100 -> {
            println("excellent")
            "A+"
        }
        in 70..80 -> {
            println("good")
            "A"
        }
        in 60..70 -> {
            println("good")
            "B"
        }
        else -> {
            println("bad")
            "C"
        }
    }

    println("another grade: $grade2")


    val mark3 = 30

    val grade3 = when {
        (mark3 > 80) -> {
            println("excellent")
            "A"
        }
        (mark >= 60) -> {
            println("good")
            "B"
        }
        else -> {
            println("not good")
            "C"
        }


    }

    println("3rd grade: $grade3")


    //==========================================
    /* loop */
    printMessage("loop")

    for (item in 1..5) {
        print("$item, ")
    }
    println()
    for (ch in "shibli") {
        println(ch)
    }

    printMessage("reverse loop")
    for (item in 8 downTo 0) {
        print("$item, ")
    }
    printMessage("step by loop")
    for (item in 8 downTo 0 step 3) {
        print("$item, ")
    }


    printMessage("0 to 8, 8 excluded")
    for (item in 0 until 8) {
        print("$item, ")
    }

    printMessage("advanced range")
    for (item in 1.until(7).step(2).reversed()) {
        print("$item, ")
    }

    println()

    for (item in 1.rangeTo(10).step(2)) {
        print("$item, ")
    }

    printMessage("loop with index")

    for ((index, item) in 1.until(7).step(2).reversed().withIndex()) {
        print("$index. => $item, ")
    }

    val x = arrayListOf<Int>(1, 2, 3, 4, 5)
    x.add(10)

    println(x)

    printMessage("loop through array")
    val myArray = arrayOf(10, 20, 30, 40)

    for (index in myArray.indices) {
        print("$index : ${myArray[index]}, ")
    }


    printMessage("function")
    println(add(2, 5))
    println(multiply(2, 5))
    println(multiplyDefault1(2, 5))
    println(multiplyDefault1(2))

    printMessage("class")

    var u = User("Tahmim", "Ahmed", "shibli05@gmail.com")
    val p1 = Person("Tahmim Ahmed", "Shibli")
    val p2 = Person("Tahmim", "Ahmed", "Shibli")

    printMessage("object")

    var location = object {
        var x = 200
        var y = 400

        fun printIt() {
            println("Position = (${x}, ${y})")
        }
    }

    location.printIt()
    location.x = 300
    location.y = 500
    location.printIt()


    val maxTemp = MySingleton.getMaxTemp()
    val minTemp = MySingleton.getMinTemp()

    println("max temp: $maxTemp, and min temp: $minTemp")

    printMessage("interface")

    val myCar = Toyota()
    myCar.go()
    println("My car's make = ${myCar.makeName}")
    myCar.stop()

    printMessage("generics")

    val maxInt: Int = max(420, 850, 670)
    val maxLong: Long = max(4242L, 8585L, 6767L)
    val maxByte: Byte = max(42.toByte(), 85.toByte(), 67.toByte())
    val maxString: String = max("Alpha", "Beta", "Gamma")

    println("The max int = $maxInt")
    println("The max long = $maxLong")
    println("The max byte = $maxByte")
    println("The max string = $maxString")

    printMessage("java from kotlin")
    val frisky = Animal("Frisky", "cat", 10)
    println(frisky.show())

    frisky.weight = 9

    //
//    frisky.kind = "cat"
    println(frisky.show())

}

object MySingleton {
    val temperatures = arrayOf(20, 30, 40, 80)
    fun getMaxTemp() = temperatures.max()
    fun getMinTemp() = temperatures.min()
}

fun foo(name: String, number: Int = 42, toUpperCase: Boolean = false) =
        (if (toUpperCase) name.toUpperCase() else name) + number

fun useFoo() = listOf(
        foo("a"),
        foo("b", number = 1),
        foo("c", toUpperCase = true),
        foo(name = "d", number = 2, toUpperCase = true)
)

fun printMessage(msg: String = "") {
    println()
    println()
    println("======================================================")
    println(msg)
    println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
}


fun add(a: Int, b: Int): Int = a + b

fun multiply(a: Int, b: Int) = a * b

fun multiplyDefault1(a: Int, b: Int = 1): Int {
    return a * b
}

