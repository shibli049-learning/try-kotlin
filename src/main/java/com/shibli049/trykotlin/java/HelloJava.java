package com.shibli049.trykotlin.java;

import com.shibli049.trykotlin.HelloWorldKt;
import com.shibli049.trykotlin.MaxKt;
import com.shibli049.trykotlin.Person;

/**
 * Created by github.com/shibli049 on 3/15/18.
 */
public class HelloJava {

  public static void main(String[] args) {
    HelloWorldKt.printMessage("kotlin from java");

    Person person = new Person("Abrar", "Ahmed");

    System.out.println(person.getFirstName());
    System.out.println(person.getMiddleName());

    System.out.println("max int: " + MaxKt.max(1,2,3));
    System.out.println("max str: " + MaxKt.max("11", "9", "8"));
  }
}
