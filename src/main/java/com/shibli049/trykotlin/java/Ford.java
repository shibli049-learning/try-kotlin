//package com.shibli049.trykotlin.java;
//
//import com.shibli049.trykotlin.Vehicle;
//import org.jetbrains.annotations.NotNull;
//
///**
// * Created by github.com/shibli049 on 3/14/18.
// */
//public class Ford implements Vehicle{
//
//  @NotNull
//  @Override
//  public String getMakeName() {
//    return "Ford";
//  }
//
//  @NotNull
//  @Override
//  public String getStartSound() {
//    return "Vrroooom";
//  }
//
//  @NotNull
//  @Override
//  public String getStopSound() {
//    return "Skeetch";
//  }
//
//  @Override
//  public int getMpg() {
//    return 30;
//  }
//
//  @Override
//  public int getDoors() {
//    return 5;
//  }
//
//
//}
